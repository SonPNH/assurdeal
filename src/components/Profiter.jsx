import React from 'react';

class Profiter extends React.Component {
	render() {
		return (
				<div id="profiter" className="profiter">
					<div className="container">
						<div className="section-title text-center">
							<h2>{this.props.data ? this.props.data.title : "loading"}</h2>
							<h5>{this.props.data ? this.props.data.child : "loading"}</h5>
						</div>
						<div className="flex-justify-center">
							<div className="col-md-6">
								{this.props.data
									? this.props.data.content.map((item, key) => (
											<div  key={key} className="col-md-6">
												<div className={"box-logo box-logo"+key}>
													<img src={"img/Assurdeal/"+ item} alt="Logo"/>	
												</div>											
											</div>
										))
									: "loading"}
							</div>
						</div>
					</div>
				</div>
        )
    }
}

export default Profiter