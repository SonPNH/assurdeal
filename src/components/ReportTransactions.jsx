import React from 'react'

class ReportTransactions extends React.Component {
	render() {
		return (
			<div id="transactions" className="transactions">
				<div className="container">
					{this.props.data ? this.props.data.map((item, i) => (
						<div key={i} className="col-md-4 items-transactions">
							<div className="border-eclise">
								<p className="total-transactions">{item.count}</p>
								<p className="title-transactions">{item.name}</p>
							</div>
						</div>
						))
					: "loading"
					}
				
				</div>
			</div>
		)
	}
}

export default ReportTransactions;