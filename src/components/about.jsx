import React, { Component } from 'react'

export class about extends Component {
  render() {
    return (
        <div id="about">
        <div className="container">
            <div className="section-title text-center">
              <h2>{this.props.data ? this.props.data.title : "loading"}</h2>
            </div>
          <div className="row flex-align-center">
            <div className="col-xs-5 col-md-5">
              <ul>
                {this.props.data ? this.props.data.Why.map(item => <li>{item}</li>)  :"loading"}
              </ul>
            </div>
            <div className="col-xs-2 col-md-2"> <img src="img/Assurdeal/image 1.png" className="img-responsive" alt=""/> </div>
            <div className="col-xs-5 col-md-5">
              <ul>
                {this.props.data ? this.props.data.Why2.map(item => <li>{item}</li>)  :"loading"}
              </ul>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default about
