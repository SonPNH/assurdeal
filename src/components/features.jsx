import React, { Component } from "react";
import ReactTable from 'react-table-6'
import 'react-table-6/react-table.css'

const rowData = [
  {
      "Features": "das",
      "Zone géo": "das",
      "Clientèle": "das",
      "Contrats": "das",
      "Commissions": "das",
      "Dossier": "das"
  },
  {
      "Features": "das",
      "Zone géo": "das",
      "Clientèle": "das",
      "Contrats": "das",
      "Commissions": "das",
      "Dossier": "das"
  },
  {
      "Features": "das",
      "Zone géo": "das",
      "Clientèle": "das",
      "Contrats": "das",
      "Commissions": "das",
      "Dossier": "das"
  },
  {
      "Features": "das",
      "Zone géo": "das",
      "Clientèle": "das",
      "Contrats": "das",
      "Commissions": "das",
      "Dossier": "das"
  },
  {
      "Features": "das",
      "Zone géo": "das",
      "Clientèle": "das",
      "Contrats": "das",
      "Commissions": "das",
      "Dossier": "das"
  }
]


export class features extends Component {
  render() {
  const {data} = this.props;
  
    return (
      <div id="features" className="text-center">
        <div className="container">
          <div className="section-title">
          <h2>{data ? data.title : "loading"}</h2>
          </div>
          <div className="row custom-table">
            <ReactTable
              columns={data ? data.columns : []}  
              data={rowData }
              showPagination= {false}
              defaultPageSize="5"
              sortable= {false}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default features;
