import React, { Component } from "react";

export class Header extends Component {
  render() {
    return (
      <header id="header">
        <div className="intro">
            <div className="container">
              <div className="row">
                <div className="col-md-8 col-md-offset-2 intro-text">
                  <img className="w-100" src="img/Assurdeal/5 clics.svg" alt="clics"/>
                </div>
              </div>
            </div>
          </div>
      </header>
    );
  }
}

export default Header;
