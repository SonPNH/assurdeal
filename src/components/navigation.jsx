import React, { Component } from "react";
import ReactFlagsSelect from 'react-flags-select';

//import css module
import 'react-flags-select/css/react-flags-select.css';

export class Navigation extends Component {
  
  onSelectFlag = (countryCode) => {
    console.log(countryCode)
  }

  render() {
    return (
      <nav id="menu" className="navbar navbar-default navbar-fixed-top">
        <div className="container">
          <div className="navbar-header">
            <button
              type="button"
              className="navbar-toggle collapsed"
              data-toggle="collapse"
              data-target="#bs-example-navbar-collapse-1"
            >
              {" "}
              <span className="sr-only">Toggle navigation</span>{" "}
              <span className="icon-bar"></span>{" "}
              <span className="icon-bar"></span>{" "}
              <span className="icon-bar"></span>{" "}
            </button>
            <a className="navbar-brand page-scroll" href="#page-top">
              <img src="img/Assurdeal/assurdeal logo default transparent.svg" alt="logo"/>
            </a>{" "}
          </div>

          <div
            className="collapse navbar-collapse"
            id="bs-example-navbar-collapse-1"
          >
            <div className="flex-justify-around">
            <ul className="nav navbar-nav">
              <li>
                <a href="#features" className="page-scroll">
                  Outils
                </a>
              </li>
              <li>
                <a href="#services" className="page-scroll">
                  Services
                </a>
              </li>
              <li>
                <a href="#portfolio" className="page-scroll">
                  Tarifs
                </a>
              </li>
            </ul>
            <div className="flex-align-center">
              <ReactFlagsSelect 
                countries={["FR", "US"]} 
                defaultCountry="FR"
                onSelect={this.onSelectFlag} 
                className="custom-flag"
                showSelectedLabel={false} 
              />
              <a className="margin-width-20" href="http://">Connexion</a>
              <button className="navbar-button">
                Accès Gratuit
              </button>
            </div>
          </div>
          </div>
        </div>
      </nav>
    );
  }
}

export default Navigation;
